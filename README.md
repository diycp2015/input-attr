# 这是啥?

> :exclamation: :exclamation: :exclamation: 本扩展暂时已基本成型, 暂时停止维护! :exclamation: :exclamation: :exclamation: 

诶嘿! 我又写了一个扩展, 哦吼! 那这个扩展能干嘛呢? 非常简单, 就是赋予表单内 `input` 和 `textarea` 更多的能力! 啥? 你问我啥能力? 看图吧!

![效果图](./imgs/thumb/thumb_01.png)

> 更好的阅读文档方式! [点击这里](https://layui-exts.gitee.io/input-attr/) 在线阅读文档!

---

# 怎么使用?

1. 下载源代码, 通常情况下, 你可以在 [releases](https://gitee.com/layui-exts/input-attr/releases) 这里找到所有已经发布的版本.
2. 将下载好的文件, 通常是压缩包, 解压到你项目的扩展目录里去, 譬如: `libs/layui_exts`
3. 确认项目的 `layui.config` 和 `layui.base` 配置是否正确, 可参考 [示例文件](./example.html)
4. 使用 `layui.use` 来引入扩展! 可参考 [示例文件](./example.html)

---

# 代码片段

> 仅供学习参考, 请勿无脑复制粘贴照搬照抄.

## 配置扩展

```javascript
layui
    .config({
        base: "./libs/layui_exts/",
    })
    .extend({
        inputAttr: "inputAttr/js/index",
    });
```

## 引入扩展并使用

### HTML 部分

```html
<div class="layui-form">
    <div class="layui-form-item">
        <div class="layui-form-label">带删除按钮</div>
        <div class="layui-input-block">
            <!-- 添加 clearable 属性, 赋予输入框删除内容的能力, 仅对 input 标签生效-->
            <input type="text" placeholder="输入点东西看看" value="作者真给力!" class="layui-input" clearable />
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-form-label">字数统计</div>
        <div class="layui-input-block">
            <!-- 添加 show-word-limit 属性, 赋予输入框统计字数+限制字数的能力-->
            <!-- input 和 textarea 都可用! 设置 show-word-limit 必须要设置 maxlength -->
            <input type="text" placeholder="输入点东西看看" value="酷啊! Nice!" maxlength="20" class="layui-input" show-word-limit />
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-form-label">字数统计</div>
        <div class="layui-input-block">
            <!--  不废话了, 同理-->
            <textarea rows="3" class="layui-textarea" placeholder="输入点东西看看" maxlength="500" show-word-limit>亲亲抱抱举高高 (づ￣3￣)づ╭❤～</textarea>
        </div>
    </div>
</div>
```

### Javascript 部分

```javascript
layui.use(["inputAttr"], function () {
    var inputAttr = layui.inputAttr;

    // 最简单的调用方式, 你啥参数都不要传递
    // 初始化一下就好了
    // 如果你铁了心要加参数也无妨, 去啃文档吧, 几分钟的事情...
    // inputAttr.init(); <- v1.x 写法, v2.x 开始变更为 render
    input.render();
});
```

---

# 可配置项

!> 讲真, 太简单了! 拢共就下面 2 个配置项! nice!

## 配置项

### root

-   根节点
-   默认: `$(document)`
-   即: 从根节点下匹配 `input,textarea` 标签然后进行处理

### matchRule

-   匹配规则
-   默认: `input,textarea`
-   在 `v1.0.1.20210818` 版本更新!
-   具体规则写法同 jquery 写法, 可参考示例页面

### event

-   托管事件
-   目前仅有一个 `clearable` 事件, 即当内容被清空的时候触发.

## 参考配置

```javascript
layui.use(["inputAttr"], function () {
    var inputAttr = layui.inputAttr;

    // 最简单的调用方式, 你啥参数都不要传递
    // 初始化一下就好了
    // 如果你铁了心要加参数也无妨, 去啃文档吧, 几分钟的事情...
    inputAttr.render({
        // 重新定义根节点
        root: ".layui-form",
        // 事件托管
        event: {
            // 当点击清除按钮内容被清理
            clearable: function (event, dom) {
                console.log({ event: event, dom: dom });
            },
        },
    });
});
```

---

# 支持作者

最简单的粗暴的方式就是直接使用 **钞能力**, 当然这是您自愿的! **点击可直接查看大图**

## 钞能力通道

<img src="./imgs/support/zhifubao.jpg" height="200px" title="支付宝">
<img src="./imgs/support/weixin.png" height="200px" title="微信">
<img src="./imgs/support/qq.png" height="200px" title="QQ">

当然, 如果客观条件不允许, 主观上不愿意, 也无伤大雅嘛! 谁的钱都是自己幸苦挣来的. 除了使用 **钞能力**, 你还可以通过以下方式支持作者!

## 打工人通道

1. 给项目点个 Star, 让更多的小伙伴知道这个扩展!
2. 积极测试, 反馈 BUG, 如果发现代码中有不合理的地方积极反馈!
3. 加入粉丝群, 看看有多少志同道合的小伙伴! <a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=SykXsUtejaedB0TSs6a0_S-SseGByzdU&jump_from=webapi">690109078</a>
